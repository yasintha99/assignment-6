#include<stdio.h>

int fibonacciSeq(int n);
int fibonacci(int n);
int i=0;

int fibonacciSeq(int n){
  if (n==0)
    return 0;
  else if (n==1)
    return 1;
  else{
    return fibonacciSeq (n-1) + fibonacciSeq(n-2);
  }
}

int fibonacci(int n){
  if (i<=n)
  {
      printf("%d\n",fibonacciSeq(i));
      i=i+1;
      fibonacci(n);
  }
}

int main(){
  int n;
  int i=0;
  printf("Enter no of terms : ");
  scanf("%d",&n);
  fibonacci(n);
  return 0;
}
