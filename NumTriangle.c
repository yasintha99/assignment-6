#include<stdio.h>

int numTriangle(int n);
int triangle (int x);
int x;
int y=1;

int numTriangle(int n){

   if (n>0)
   {
     triangle(y);
     printf("\n");
     y=y+1;
     numTriangle(n-1);
     return 0;
   }
}

int triangle(int x){

   if(x>=1)
   {
     printf("%d",x);
     triangle(x-1);
   }
}

int main(){

  int n;
  int x=0;
  printf("Enter no of terms : ");
  scanf("%d",&n);
  numTriangle(n);
}
